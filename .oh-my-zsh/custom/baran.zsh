# Add yourself some shortcuts to projects you often work on
# Example:
#
# brainstormr=/Users/robbyrussell/Projects/development/planetargon/brainstormr

# scm_breeze
[ -s "$HOME/.scm_breeze/scm_breeze.sh" ] && . "$HOME/.scm_breeze/scm_breeze.sh"

# suffix aliases, from http://grml.Org/zsh/zsh-lovers.html
alias -s tex=vim
alias -s java=vim

# just enter in a URL on the command line and it will load through w3m
alias -s html=w3m
alias -s org=w3m

alias rm="rm -iv"
alias cp="cp -iv"
alias mv="mv -iv"
alias t2="titlebar2"
alias sls="screen -ls"
alias t="truecrypt -u"
alias td="truecrypt -d"
alias scpresume="rsync --partial --progress --rsh=ssh"

alias la="ls -Gla"
alias ll="ls -Gl"
alias ls="ls -G"

alias hg='fc -l 0|egrep'

# Global aliases - Do not have to be at the beginning of the command line. http://zshwiki.org
alias -g H='| head'
alias -g T='| tail'
alias -g G='| egrep'
alias -g L="| less"
alias -g M="| most"
alias -g B="&|"
alias -g HL="--help"
alias -g LL="2>&1 | less"
alias -g CA="2>&1 | cat -A"
alias -g NE="2> /dev/null"
alias -g NUL="> /dev/null 2>&1"

alias grep="grep --color=auto"

function ffind() {
   /usr/bin/find -L $1 -iname $2 2> /dev/null
}

function efind() {
   /usr/bin/find -L $1 -iname $2 -exec ${@:3} -- {} + 2> /dev/null
}

# automatically do a pushd on cd
setopt auto_pushd

# if exit value is non-zero, print it
setopt print_exit_value

# autocompletion heaven!
autoload -U compinit
compinit

# menu style autocompletion
zmodload zsh/complist
zstyle ':completion:*:default' list-prompt '%S%M matches%s'

# make 'q' stop listscroll
bindkey -M listscroll q send-break

# select 0 specifies we should always do menu selection, do 'long' to start menu when more than a page of results is available
zstyle ':completion*:default' menu 'select=0'

# when in menu selection, ust Ctrl-a to add more things from the menu to the prompt
bindkey -M menuselect '\C-a' accept-and-menu-complete

# to add more hosts, seperate by a space
compctl -k hosts telnet ftp ssh scp

## History changes
# consecutive duplicates are ignored
setopt hist_ignore_dups

# expires duplicates first when $HISTSIZE is reached
# HISTSIZE should be larger than SAVEHIST to make this
# work, otherwise pruning won't happen
setopt hist_expire_dups_first

# make all terminals share history
setopt SHARE_HISTORY

# Extended history sounds cool, unique to zsh
setopt extended_history

# Turn on spell checking (this only checks commands, faster than correct_all which checks filenames too)
#setopt correct

setopt autocd
setopt noclobber
setopt hist_allow_clobber

#bindkey '\e[1~' beginning-of-line
#bindkey '\e[4~' end-of-line

bindkey '^R' history-incremental-search-backward
# bindkey '^[[H' beginning-of-line
# bindkey '^[[F' end-of-line 
# bindkey '^[[3~' delete-char 
#bindkey 'ÿ'     backward-delete-word
#bindkey '®'     insert-last-word
#bindkey '' vi-backward-kill-word
#bindkey -s '\e[1;5A' "cd .."
#bindkey -s '\e[1;5B' "ll"
#bindkey '\e0D' backward-word  
#bindkey '\e[1;5C' forward-word
# 
# case $TERM in (rxvt*)
#    bindkey '\e[7~' beginning-of-line
#    bindkey '\e[8~' end-of-line 
#    bindkey '\e[3~' delete-char 
#    bindkey '' vi-backward-kill-word
#    bindkey -s '\eOa' "cd .."
#    bindkey -s '\eOb' "ll"
#    bindkey '\eOd' backward-word  
#    bindkey '\eOc' forward-word  ;;
# esac

export PAGER=less

# watch people login/logout
# watch=(all)
#export WATCHFMT="[01;37m%n@%m %(a.[01;31mLOGGED IN.[01;32mLOGGED OUT) [01;37mat %t on term %l"
#export LOGCHECK=0

# Make all my shells use the same hist file
export HISTFILE=~/.zsh_history
export HISTSIZE=7500
export SAVEHIST=5000

# Attempt to change titlebar
titlebar () {
   HOSTNAME=`uname -n`
   if [ "$TERM" = "xterm" ] || [ "$TERM" = "rxvt" ]
   then
      ilabel () { /bin/echo -n "]1;$*"; }
      label ()  { /bin/echo -n "]2;$*"; }
      alias stripe='label $HOSTNAME - ${PWD#$HOME/}'  
      alias stripe2='label $HOSTNAME - vi $*'
      cds () 
      { 
        if [ -z "$1" ]
        then
            "cd"
        else
            "cd" $*
        fi
        eval stripe; 
      }
      vis () { eval stripe2; "vim" $*; eval stripe; }
      alias cd=cds
      alias v=vis
      alias vi=vis
      alias vim=vis
      eval stripe
      eval ilabel "$HOSTNAME"
   fi  
}
titlebar

titlebar2() {
   echo -ne "\033]0;$@\007"
}

## Change the window title of X terminals 
case $TERM in
	xterm*|rxvt|Eterm|eterm)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\007"'
		;;
	screen)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/$HOME/~}\033\\"'
		;;
esac

    ###
    # Decide if we need to set titlebar text.

# nuking all prompt stuff and moving to zsh_prompt instead
# # left hand side prompt
# PROMPT=$'%T %B%#%b '
# 
# # right hand side prompt
# # I'm leaving the following old RPROMPT in because it has a good example of zsh ternary prompt, man zshall
# #RPROMPT=$'[%B%~%b] %h%(4L. +.)%L%'
# RPROMPT=$'%m [%B%~%b] %h'


#case $TERM in
#	xterm*)
#   PR_TITLEBAR=$'%{\e]0;%(!.-=*[ROOT]*=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\a%}'
#   ;;
#	screen)
#   PR_TITLEBAR=$'%{\e_screen \005 (\005t) | %(!.-=[ROOT]=- | .)%n@%m:%~ | ${COLUMNS}x${LINES} | %y\e\\%}'
#	;;
#	*)
#   PR_TITLEBAR=''
#	;;
#esac
    
    
    ###
    # Decide whether to set a screen title
#if [[ "$TERM" == "screen" ]]; then
#   PR_STITLE=$'%{\ekzsh\e\\%}'
#else
#	PR_STITLE=''
#fi
#export PR_STITLE
#export PR_TITLEBAR

#chpwd() {
#   [[ -t 1 ]] || return
#   case $TERM in
#      *xterm*|rxvt|(dt|k|E)term) print -Pn "\e]2;%~\a"
#         ;;
#   esac
#}

##uncomment the following to activate bash-completion:
#[ -f /etc/profile.d/bash-completion ] && source /etc/profile.d/bash-completion

## less stuff
# Decode directories:
if [ -d "$1" ]; then
    echo "$1:"; ls -l $1
fi

# view strings inside of an executable
if [ -x "$1" ]; then
    type=$(file "$1")
    case "$type" in 
      *executable* )
        echo -e "$type\n"
        strings "$1"
        ;;
    esac
fi

#keep us in emacs mode
bindkey -e

# this is mostly for mac os x
[ -s "~/.oh-my-zsh/custom/zsh.kbd" ] && source ~/.zsh_kbd

# varies for each machine
[ -s "~/.oh-my-zsh/custom/hosts.zsh" ] && source ~/.oh-my-zsh/custom/hosts.zsh
[ -s "~/.scm_breeze/scm_breeze.sh" ] && source "/Users/barann/.scm_breeze/scm_breeze.sh"
