" NOTE: this vimrc uses vundle.  quickinstall:
" % git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
" % vim
" % :BundleInstall

" toggle wrap mode
nmap ,,w :set wrap!<CR>

" toggle paste mode
nmap ,,p :set paste!<CR>

" toggle case insensitivity
nmap ,,i :set ic!<CR>

" toggle line numbers
:nmap ,,l :setlocal number!<CR>

" highlight while search
set hlsearch

" but provide an easy way to toggle it
nmap ,,/ :set hlsearch!<CR>

let g:EasyMotion_leader_key = ','

set nocompatible
"call pathogen#infect()
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" My Bundles here:
"
" original repos on github
Bundle 'tpope/vim-fugitive'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
"Bundle 'tpope/vim-rails.git'
" vim-scripts repos
Bundle 'L9'
Bundle 'FuzzyFinder'
" non github repos
Bundle 'kien/ctrlp.vim'
"Bundle 'davidbeckingsale/writegood.vim'
Bundle 'mbbill/undotree'

" too slow, removing
"Bundle 'klen/python-mode.git'

filetype plugin indent on

"" This is for CtrlP plugin

" use git ls-files to generate file list
let g:ctrlp_user_command = {
        \ 'types': {
                \ 1: ['.git', 'cd %s && git ls-files --exclude-standard -co'],
                \ 2: ['.hg', 'hg --cwd %s locate -I .'],
                \ },
        \ 'fallback': 'find %s -type f'
        \ }

                "\ 1: ['.git', 'cd %s && git ls-files --exclude-standard -co|grep -vEf ~/.gitskip'],
" ignore these types of files
"" note I'm handling this in the ~/.gitskip file
"let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn|class|jar)$'


"Syntax highlighting only for enhanced-vi
"if has("mouse_gpm") && filereadable("/usr/share/vim/vim64/syntax/syntax.vim")
  syntax on
"endif

"For Eclim to work
"filetype plugin indent on

"To hide the toolbar in the gui
set guioptions-=T

"" Explorer
" Make splits vertical
"   bad b/c you don't have the option to specify hor if you enable this
"let g:explVertical=1

" Make explorer on left
let g:explStartRight=0

" Make file size/date/etc showup by default
let g:explDetailedList=1

" Set the size to something reasonable, default 15
let g:explWinSize=90

" this makes 'scons' the default make program
"set makeprg=kscons

"" MRU
" Exclude the following files from being added to the history
let MRU_Exclude_Files = 'tips|scratch|cheatsheet.txt|status'

" Window height
let MRU_Window_Height = 20

" Don't auto close it after selecting a file to open
let MRU_Auto_Close = 0

" Keep last 20 opened files
let MRU_Max_Entries = 20

"Set a statusbar
set statusline=~

"" Taglist
" This is for exuberant-ctags and taglist to work
filetype on

" So I can see the prototype
" let Tlist_Display_Prototype = 1

" Window width fixed to
let Tlist_WinWidth = 30

" Don't resize the width
let Tlist_Inc_Winwidth = 0

"" selectbuf
" Remap the selectbuf command
nmap <silent> <unique> \sb <Plug>SelectBuf

"I know it's horrible for a vi master but useful for newbies.
imap <C-a> <Esc>I
imap <C-e> <ESC>A
map <C-Tab> <C-W>w
imap <C-Tab> <C-O><C-W>w
cmap <C-Tab> <C-C><C-Tab>

imap <M-BS> <Esc>db
imap <C-BS> <Esc>db

" Make this buffer a scratch buffer
"map <silent> <C-a> :setlocal buftype=nofile<C-M>:setlocal bufhidden=hide<C-M>:setlocal noswapfile<C-M>:setlocal buflisted<C-M>

"Map that scroll wheel! http://vimdoc.sourceforge.net/htmldoc/scroll.html#xterm-mouse-wheel
"map <M-Esc>[62~ <MouseDown>
"map! <M-Esc>[62~ <MouseDown>
"map <M-Esc>[63~ <MouseUp>
"map! <M-Esc>[63~ <MouseUp>
"map <M-Esc>[64~ <S-MouseDown>
"map! <M-Esc>[64~ <S-MouseDown>
"map <M-Esc>[65~ <S-MouseUp>
"map! <M-Esc>[65~ <S-MouseUp>

"Some macros to manage the buffer of vim
map <F5> :bp<C-M>
map <F6> :bn<C-M>
map <F7> :bd<C-M>


" Ctrl+{1..5} for tabs 1..5
nmap <C-1> :1tabnext<cr>
nmap <C-2> :2tabnext<cr>
nmap <C-3> :3tabnext<cr> 
nmap <C-4> :4tabnext<cr>
nmap <C-5> :5tabnext<cr>

"Make it so I can use the mouse to resize splits
set mouse=

"Modify the status line to display some more useful information
"Note - this only shows up in one or more window splits
"set statusline=[%02n]\ %F%(\ %r%y%m%)\ %=%c%V,\ %l/%L\ \ %P
set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P

"Default backspace like normal
set bs=2

"Map keys for MultipleSearch
"h used to be left
map  :Search 
"j used to be down
map j gj
"k used to be up
map k gk
"l used to be right
map <C-h> :SearchReset<C-M>

"Make control-up go up by 20 lines
map <C-k> 20k

"Make control-down go down by 20 lines
map <C-j> 20j

"Get rid of trailing whitespaces in the whole document
map <C-l> :%s/\s\+$//
"Terminal for 80 char ? so vim can play till 79 char.
"set textwidth=79

"Some option desactivate by default (remove the no).
set nobackup

" incremental search
set incsearch

" case insenstive searching
set ignorecase

" UNLESS you have something capitlized
set smartcase

"Display a status-bar.
"set laststatus=2

"attempt to have an edge guide for column 80
"highlight LineOverflow guibg=#202060 guifg=white
"autocmd CursorHold * match LineOverflow "\%61v.*"

set shiftwidth=3
set softtabstop=3
set expandtab
set tabstop=8

" show the line number
set number

au BufNewFile,BufRead *.h setf c
au BufNewFile,BufRead *.mk setf make
                                                                                          
au BufEnter * set vb t_vb=
au BufEnter * if &readonly | setl nomodifiable | endif
au BufEnter *.log setl readonly nomodifiable
"au FileType make,conf set noexpandtab nosmarttab
set autoindent
set smartindent
set shiftwidth=3
"set textwidth=79
"set smarttab
set formatoptions=2ntcroqlv
set cinoptions=g0,t0,c1s,(0,m1,l1
set cinwords+=case
let c_comment_strings=1


"iSpell map to V
"map  :w:!ispell % :e!
:autocmd FileType mail :nmap <C-\> :w<CR>:!aspell -e -c %<CR>:e<CR>

"Choose a good colorscheme
"color peachpuff
"color 256_asu1dark
"color macvim
set background=dark

if $COLORTERM == 'gnome-terminal'
   "Remap Ctrl+W+<ArrowKey> to Alt+<ArrowKey>
   map O3D <C-W>h
   map O3B <C-W>j
   map O3A <C-W>k
   map O3C <C-W>l
else
   "Remap (aterm) Ctrl+W+<ArrowKey> to Ctrl+<ArrowKey>
   "Below is for aterm
   "map Od <C-W>h
   "map Ob <C-W>j
   "map Oa <C-W>k
   "map Oc <C-W>l

   "This is for xterm
   "map [1;5D <C-W>h
   "map [1;5B <C-W>j
   "map [1;5A <C-W>k
   "map [1;5C <C-W>l

   map OB <C-W>j
   map OA <C-W>k
   map OC <C-W>l
   map OD <C-W>h
endif

"Show the position of the cursor.
set ruler

"no wrap
set nowrap

"Show matching parenthese.
"set showmatch

"" Gzip and Bzip2 files support
" Take from the Debian package and the exemple on $VIM/vim_exemples
if has("autocmd")

" Set some sensible defaults for editing C-files
augroup cprog
  " Remove all cprog autocommands
  au!

  " When starting to edit a file:
  "   For *.c and *.h files set formatting of comments and set C-indenting on.
  "   For other files switch it off.
  "   Don't change the order, it's important that the line with * comes first.
  autocmd BufRead *       set formatoptions=tcql nocindent comments&
  autocmd BufRead *.c,*.h set formatoptions=croql cindent comments=sr:/*,mb:*,el:*/,://
augroup END

" Also, support editing of gzip-compressed files. DO NOT REMOVE THIS!
" This is also used when loading the compressed helpfiles.
augroup gzip
  " Remove all gzip autocommands
  au!

  " Enable editing of gzipped files
  "	  read:	set binary mode before reading the file
  "		uncompress text in buffer after reading
  "	 write:	compress file after writing
  "	append:	uncompress file, append, compress file
  autocmd BufReadPre,FileReadPre	*.gz set bin
  autocmd BufReadPre,FileReadPre	*.gz let ch_save = &ch|set ch=2
  autocmd BufReadPost,FileReadPost	*.gz '[,']!gunzip
  autocmd BufReadPost,FileReadPost	*.gz set nobin
  autocmd BufReadPost,FileReadPost	*.gz let &ch = ch_save|unlet ch_save
  autocmd BufReadPost,FileReadPost	*.gz execute ":doautocmd BufReadPost " . %:r

  autocmd BufWritePost,FileWritePost	*.gz !mv <afile> <afile>:r
  autocmd BufWritePost,FileWritePost	*.gz !gzip <afile>:r

  autocmd FileAppendPre			*.gz !gunzip <afile>
  autocmd FileAppendPre			*.gz !mv <afile>:r <afile>
  autocmd FileAppendPost		*.gz !mv <afile> <afile>:r
  autocmd FileAppendPost		*.gz !gzip <afile>:r
augroup END

augroup bzip2
  " Remove all bzip2 autocommands
  au!

  " Enable editing of bzipped files
  "       read: set binary mode before reading the file
  "             uncompress text in buffer after reading
  "      write: compress file after writing
  "     append: uncompress file, append, compress file
  autocmd BufReadPre,FileReadPre        *.bz2 set bin
  autocmd BufReadPre,FileReadPre        *.bz2 let ch_save = &ch|set ch=2
  autocmd BufReadPost,FileReadPost      *.bz2 set cmdheight=2|'[,']!bunzip2
  autocmd BufReadPost,FileReadPost      *.bz2 set cmdheight=1 nobin|execute ":doautocmd BufReadPost " . %:r
  autocmd BufReadPost,FileReadPost      *.bz2 let &ch = ch_save|unlet ch_save

  autocmd BufWritePost,FileWritePost    *.bz2 !mv <afile> <afile>:r
  autocmd BufWritePost,FileWritePost    *.bz2 !bzip2 <afile>:r

  autocmd FileAppendPre                 *.bz2 !bunzip2 <afile>
  autocmd FileAppendPre                 *.bz2 !mv <afile>:r <afile>
  autocmd FileAppendPost                *.bz2 !mv <afile> <afile>:r
  autocmd FileAppendPost                *.bz2 !bzip2 -9 --repetitive-best <afile>:r
augroup END

endif " has ("autocmd")

"added for cscope
"source ~/.vim/cscope_maps.vim

""end added for scope
